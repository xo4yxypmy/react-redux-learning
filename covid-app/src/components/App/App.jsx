import { useEffect, useState } from 'react'
import Cards from '../Cards/Cards'
import styles from './App.module.scss'
import { getGlobalData } from '../../api/api'

const App = () => {
  const [cardData, setCardData] = useState({})
  useEffect(() => {
    ;(async () => {
      const data = await getGlobalData()
      if (data) setCardData(data?.Global)
    })()
  }, [])
  return (
    <div className={styles.App}>
      <Cards globalData={cardData} />
    </div>
  )
}

export default App
