import React from 'react'
import styles from './Cards.module.scss'

const Cards = ({ globalData }) => {
  console.log(globalData)
  const date = new Date(globalData?.Date)
  return (
    <div className={styles.Cards}>
      <h1>Infeacted</h1>
      <span>{globalData?.NewConfirmed}</span>
      <p>{`${date.getDay()}.${date.getMonth()}.${date.getFullYear()}`}</p>
      <p>Data from ...</p>
    </div>
  )
}

export default Cards
