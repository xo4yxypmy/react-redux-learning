import React from 'react'
import ReactDOM from 'react-dom'
import App from './components/App/App'
import './sass/index.scss'

ReactDOM.render(<App />, document.getElementById('root'))
