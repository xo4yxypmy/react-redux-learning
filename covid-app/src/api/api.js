import axios from 'axios'
import { API_URL, API_SUMMARY, API_FIND_COUNTRY } from './apiConstants'

const getGlobalData = async () => {
  try {
    const { data } = await axios.get(`${API_URL}${API_SUMMARY}`)
    return data
  } catch (error) {
    return false
  }
}

export { getGlobalData }
