const API_URL = 'https://api.covid19api.com'
const API_SUMMARY = '/summary'
const API_FIND_COUNTRY = '/country'

export { API_URL, API_SUMMARY, API_FIND_COUNTRY }
