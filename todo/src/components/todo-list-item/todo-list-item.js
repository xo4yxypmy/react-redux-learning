import React from 'react'

import './todo-list-item.css'

const TodoListItem = ({ label, important = false }) => {
  const style = {
    color: important ? 'steelblue' : 'balck',
    fontWeight: important ? 'bold' : 'normal'
  }
  return (
    <span className="todo-list-item">
      <span className="todo-list-item-label" style={style}>
        {label}
      </span>

      <button type="button" className="btn btn-outline-warning btn-sm float-right">
        <i className="fa fa-star-o"></i>
      </button>

      <button type="button" className="btn btn-outline-danger btn-sm float-right">
        <i className="fa fa-trash-o"></i>
      </button>
    </span>
  )
}

export default TodoListItem
